$(document).ready(function() {
  
  // Burger
  
  $('.burger-wrap').click(function(){
    $(this).children().toggleClass('active');
    $('.nav').toggleClass('active');
    return false;
  });
  
  //Program
  
  $('.info-item-head').click(function(){
    
    var content = $(this).next();
    var parent = $(this).parent();
    
    if( parent.hasClass('active') ) {
      content.slideUp();
      parent.removeClass('active');
    } 
    else {
      $('.info-item-content').slideUp();
      $('.info-item').removeClass('active');
      content.slideDown();
      parent.addClass('active');
    }
  });
  
});





