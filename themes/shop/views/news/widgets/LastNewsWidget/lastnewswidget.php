<?php Yii::import('application.modules.news.NewsModule'); ?>
<div class="main-news-wrap">
    <div class="container">
        <div class="main-news">
            <div class="head">
                <p>Новости для жителей</p>
            </div>
            <div class="main-news-items">
                <?php if (isset($models) && $models != []): ?>
                    <?php
                    /** @var News $model */
                    foreach ($models as $model): ?>
                        <div class="main-news-item">
                            <img src="<?= Yii::app()->getTheme()->getAssetsUrl() ?>/images/news.png" alt="">
                            <div class="main-news-content">
                                <span><?= Yii::app()->dateFormatter->format("dd MMMM yyyy", $model->date) ?></span>
                                <h2 class="main-news__head"><?= $model->title ?></h2>
                                <?= $model->short_text . CHtml::link('Читать дальше', ['/news/news/view/', 'slug' => $model->slug]); ?>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
            <div class="link main-news-link">
                <a href="<?= Yii::app()->createUrl('/news') ?>">Перейти к новостям</a>
            </div>
        </div>
    </div>
</div>