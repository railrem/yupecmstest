<?php
/* @var $data News */
$url = Yii::app()->createUrl('/news/news/view', ['slug' => $data->slug]);
?>
<div class="main-news-item">
    <img src="images/news.png" alt="">
    <div class="main-news-content">
        <span>24 Октября 2017</span>
        <h2 class="main-news__head"><?=CHtml::encode($data->title)?></h2>
        <p><?= $data->short_text ?></p>
        <?= CHtml::link(Yii::t('NewsModule.news', 'read...'), $url, ['class' => 'btn btn_primary']); ?>
    </div>
</div>