<?php
/** @var string title */
/** @var CDataProvider $dataProvider */
$this->title = Yii::t('NewsModule.news', 'News');
$this->breadcrumbs = [Yii::t('NewsModule.news', 'News')];
?>

<!-- News -->

<div class="news-wrap">
    <div class="container">
        <div class="news">
            <div class="head news-head">
                <p><?= Yii::t('NewsModule.news', 'News'); ?></p>
            </div>
            <div class="main-news-items">
                <?php $this->widget(
                    'zii.widgets.CListView',
                    [
                        'dataProvider' => $dataProvider,
                        'itemView' => '_item',
                        'template' => "{items}\n{pager}",
                        'summaryText' => '',
                        'enableHistory' => true,
                        'cssFile' => false,
                        'pagerCssClass' => 'pagination',
                        'pager' => [
                            'header' => '',
                            'prevPageLabel' => '<li class="pagination__arrows">
                                <img src="' . Yii::app()->theme->getAssetsUrl() . '/images/pagination__arrow1.png" alt="">
                        </li>',
                            'nextPageLabel' => '<li class="pagination__arrows">
                                <img src="' . Yii::app()->theme->getAssetsUrl() . '/images/pagination__arrow2.png" alt="">
                        </li>',
                            'selectedPageCssClass' => 'active',
                            'firstPageLabel' => false,
                            'lastPageLabel' => false,
                            'htmlOptions' => [
                                'class' => 'pagination'
                            ]
                        ]
                    ]
                ); ?>
            </div>

        </div>
    </div>
</div>