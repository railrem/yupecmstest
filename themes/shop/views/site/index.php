<!-- Showcase -->

<div class="showcase-wrap">
    <div class="container">
        <div class="showcase">
            <div class="showcase-items">
                <div class="showcase-item">
                    <img src="<?= $this->mainAssets ?>/images/showcase-item1.png" alt="">
                    <p>Индивидуальный подход</p>
                </div>
                <div class="showcase-item">
                    <img src="<?= $this->mainAssets ?>/images/showcase-item2.png" alt="">
                    <p>Профессиональные специалисты</p>
                </div>
                <div class="showcase-item">
                    <img src="<?= $this->mainAssets ?>/images/showcase-item3.png" alt="">
                    <p>Автоматизация всех процессов</p>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Main-news -->

<?php if (Yii::app()->hasModule('news')): ?>
    <?php $this->widget('application.modules.news.widgets.LastNewsWidget', [
        'limit' => 3
    ]); ?>
<?php endif; ?>

<!-- About -->

<div class="about-wrap">
    <div class="container">
        <div class="about">
            <div class="about-content">
                <div class="head about-head">
                    <p>Управляющая компания «Оазис»</p>
                </div>
                <span>Общество с ограниченной ответственностью Управляющая компания «Оазис». Мы работаем ради того, чтобы Вы чувствовали себя максимально комфортно. Для нас очень важно Ваше спокойствие. Мы бережно относимся к окружающей среде и обеспечиваем порядок на территории. Кроме этого, мы делаем жизнь в наших домах уютной и разнообразной. </span>
                <div class="link about-link">
                    <a href="#">Больше о компании</a>
                </div>
            </div>
            <div class="about-img">
                <img src="<?= $this->mainAssets ?>/images/about.jpg" alt="">
            </div>
        </div>
    </div>
</div>

<!-- Service -->

<div class="service-wrap">
    <div class="container">
        <div class="service">
            <div class="head service-head">
                <p>Полезные сервисы</p>
            </div>
            <div class="service-items">
                <div class="service-item">
                    <div class="service-item-img"></div>
                    <a href="#" class="service-item__head">Показания счетчиков</a>
                    <p>Ввести показания счетчиков электроэнергии, горячей и холодной воды</p>
                    <div class="link service-link">
                        <a href="#">Перейти</a>
                    </div>
                </div>
                <div class="service-item">
                    <div class="service-item-img service-item-img2"></div>
                    <a href="#" class="service-item__head">Подать заявку</a>
                    <p>Если вы являетесь жителем наших домов, то здесь вы можете оставить заявку на ремонт и
                        обслуживание</p>
                    <div class="link service-link">
                        <a href="#">Перейти</a>
                    </div>
                </div>
                <div class="service-item">
                    <div class="service-item-img service-item-img3"></div>
                    <a href="#" class="service-item__head">Оплатить квитанцию</a>
                    <p>Вы можете оплатить все услуги ЖКХ, не выходя из дома</p>
                    <div class="link service-link">
                        <a href="#">Перейти</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>