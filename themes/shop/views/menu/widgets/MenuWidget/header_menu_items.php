<div class="nav-wrap">
    <div class="container">
        <div class="burger-wrap">
            <a href="#"><span></span></a>
        </div>
        <div class="nav">
            <ul>
                <?php foreach ($this->params['items'] as $item): ?>
                    <?php
                    $url = (is_array($item['url'])) ? array_shift($item['url']) : $item['url'];
                    ?>
                    <?php if ($item['visible']): ?>
                        <li <?= ($url == '/'.Yii::app()->request->pathInfo) ? 'class="active"' : '' ?>>
                            <?= CHtml::link(CHtml::encode($item['label']), $url); ?>
                        </li>
                    <?php endif; ?>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
</div>