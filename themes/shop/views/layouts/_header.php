<!-- Header -->

<div class="header-wrap">
    <div class="container">
        <div class="header cf">
            <div class="logo-wrap">
                <a href="#" class="logo">
                    <img src="<?= $this->mainAssets ?>/images/logo.png" alt="">
                </a>
                <b>Управляющая компания</b>
            </div>
            <div class="header-items">
                <div class="header-item">
                    <b>ул. Седова, д. 20</b>
                    <span>Пн-Пт с 8:00 до 17:00</span>
                </div>
                <div class="header-item">
                    <a href="tel:79872239587">+7 987 223 95 87</a>
                    <span>Диспетчерская служба</span>
                </div>
            </div>
            <div class="link header-link">
                <a href="#">Узнать о задолженности</a>
            </div>
        </div>
    </div>
</div>

<!-- Nav -->

<?php if (Yii::app()->hasModule('menu')): ?>
    <?php $this->widget('application.modules.menu.widgets.MenuWidget', [
        'name' => 'top-menu',
        'layout' => 'header_menu_items'
    ]); ?>
<?php endif; ?>
