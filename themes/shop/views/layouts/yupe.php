<!DOCTYPE html>
<html lang="<?= Yii::app()->getLanguage(); ?>">

<head>
    <?php
    \yupe\components\TemplateEvent::fire(ShopThemeEvents::HEAD_START);

    Yii::app()->getClientScript()->registerCssFile($this->mainAssets . '/styles/slick.css');
    Yii::app()->getClientScript()->registerCssFile($this->mainAssets . '/styles/fonts.css');
    Yii::app()->getClientScript()->registerCssFile($this->mainAssets . '/styles/style.css');
    Yii::app()->getClientScript()->registerCssFile($this->mainAssets . '/styles/media.css');


    Yii::app()->getClientScript()->registerCoreScript('jquery');
    ?>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta http-equiv="Content-Language" content="ru-RU"/>
    <meta http-equiv="viewport" content="width=1200"/>
    <meta http-equiv="imagetoolbar" content="no"/>
    <meta http-equiv="msthemecompatible" content="no"/>
    <meta http-equiv="cleartype" content="on"/>
    <meta http-equiv="HandheldFriendly" content="True"/>
    <meta http-equiv="format-detection" content="address=no"/>
    <meta http-equiv="apple-mobile-web-app-capable" content="yes"/>
    <meta http-equiv="apple-mobile-web-app-status-bar-style" content="black-translucent"/>


    <title><?= $this->title; ?></title>
    <meta name="description" content="<?= $this->description; ?>"/>
    <meta name="keywords" content="<?= $this->keywords; ?>"/>
    <?php if ($this->canonical): ?>
        <link rel="canonical" href="<?= $this->canonical ?>"/>
    <?php endif; ?>

    <script type="text/javascript">
        var yupeTokenName = '<?= Yii::app()->getRequest()->csrfTokenName;?>';
        var yupeToken = '<?= Yii::app()->getRequest()->getCsrfToken();?>';
    </script>
    <?php \yupe\components\TemplateEvent::fire(ShopThemeEvents::HEAD_END); ?>
</head>
<body>
<?php \yupe\components\TemplateEvent::fire(ShopThemeEvents::BODY_START); ?>
<?php $this->renderPartial('//layouts/_header'); ?>
<?= $content ?>
<?php $this->renderPartial('//layouts/_footer'); ?>
<?php \yupe\components\TemplateEvent::fire(ShopThemeEvents::BODY_END); ?>
<div class='notifications top-right' id="notifications"></div>
<?php
Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/js/common.js', CClientScript::POS_END);
Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/js/jquery-3.2.1.min.js', CClientScript::POS_END);
Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/js/slick.min.js', CClientScript::POS_END);
?>
</body>
</html>