<div class="footer-wrap">
    <div class="container">
        <div class="footer cf">
            <div class="logo-wrap">
                <a href="#" class="logo">
                    <img src="<?= $this->mainAssets ?>/images/footer-logo.png" alt="">
                </a>
                <b>Управляющая компания</b>
            </div>
            <div class="header-items">
                <div class="header-item">
                    <b>ул. Седова, д. 20</b>
                    <span>Пн-Пт с 8:00 до 17:00</span>
                </div>
                <div class="header-item">
                    <a href="tel:79872239587">+7 987 223 95 87</a>
                    <span>Диспетчерская служба</span>
                </div>
            </div>
            <div class="link header-link">
                <a href="#">Узнать о задолженности</a>
            </div>
        </div>
    </div>
</div>